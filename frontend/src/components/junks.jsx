import React from "react";
import bottom from "../assets/bottom.svg";

function App() {
  return (
    <div>
      <img className="w-3/4 self-center object-contain" src={bottom} alt="" />
    </div>
  );
}

export default App;
